import 'package:booktickers/screens/hotel_screen.dart';
import 'package:booktickers/screens/ticket_view.dart';
import 'package:booktickers/utils/app_info_list.dart';
import 'package:booktickers/utils/app_styles.dart';
import 'package:fluentui_icons/fluentui_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Styles.bgColor,
      body: ListView(
        children: [
          Container(
            child: Column(
              children: [
                const Gap(40),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: const DecorationImage(
                              image: AssetImage("assets/uk.png"),
                              fit: BoxFit.cover),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text('صبح بخیر', style: Styles.headLineStyle3),
                          const Gap(5),
                          Text(
                            'کتابچه بلیط',
                            style: Styles.headLineStyle1,
                          ),
                        ],
                      ),

                    ],
                  ),
                ),
                const Gap(25),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: const Color(0xFFF4F6FD)),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 12, vertical: 12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text('جستجو', style: Styles.headLineStyle4),
                        const Icon(FluentSystemIcons.ic_fluent_search_regular,
                            color: Color(0xFFBFC205)),
                      ],
                    ),
                  ),
                ),
                const Gap(40),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          print("You are tapped");
                        },
                        child: Text('مشاهده همه',
                            style: Styles.textStyle
                                .copyWith(color: Styles.primaryColor)),
                      ),
                      Text('پروازها', style: Styles.headLineStyle2),
                    ],
                  ),
                ),
                const Gap(15),
                const SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  padding: EdgeInsets.only(left: 20),
                  child: Row(
                    children: [
                      TicketView(),
                      TicketView(),
                    ],
                  ),
                ),
                const Gap(15),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          print("You are tapped");
                        },
                        child: Text('مشاهده همه',
                            style: Styles.textStyle
                                .copyWith(color: Styles.primaryColor)),
                      ),
                      Text('هتل ها', style: Styles.headLineStyle2),
                    ],
                  ),
                ),
                const Gap(15),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  padding: EdgeInsets.only(left: 20),
                  child: Row(
                      children:
                          hotelList.map((h) => HotelScreen(hotel: h)).toList()),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
