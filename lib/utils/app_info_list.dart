List<Map<String,dynamic>> hotelList =[
  {
    'image' : 'germany.png',
    'place' : 'هتل آلمان',
    'destination': 'آلمان',
    'price' : 49
  },
  {
    'image' : 'greece.png',
    'place' : 'هتل یونان',
    'destination': 'یونان',
    'price' : 81
  },
  {
    'image' : 'kenya.png',
    'place' : 'هتل کنیا',
    'destination': 'کنیا',
    'price' : 59
  },
  {
    'image' : 'uk.png',
    'place' : 'هتل انگلستان',
    'destination': 'انگلستان',
    'price' : 26
  },
];